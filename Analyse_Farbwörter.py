import os
import re
import matplotlib.pyplot as plt

# Import & Einlesen der Textdateien aus Datenstruktur
ordner_pfade = {"maennlich": r"D:\Daten\Python_Projekte\fd_abstrakte-nomen\texte\maennlich",
                "weiblich": r"D:\Daten\Python_Projekte\fd_abstrakte-nomen\texte\weiblich"} #Pfade abändern
texte = {
    "maennlich": {},  # im dictionary später { <Dateiname>: Textstring }
    "weiblich": {},

}

# lade alle texte in die vorher vorbereiteten dictionaries in 'texte'
for key, item in ordner_pfade.items():
    for file in os.scandir(item):
        with open(file.path, "r", encoding="utf-8") as f:
            textinhalt = f.read()
        texte[key][file.name] = textinhalt


#  Textbearbeitung -> split / strip / lower
def clean_text(text: str) -> list[str]:
    """
    Säubert einen Text und gibt die Tokens zurück
    :param text:
    :return:
    """

    text = text.replace("ſ", "s")
    text = text.replace("", "")
    text = text.replace("-\n", "")
    text = re.sub("\[\d+(\/?\d+)\]", "", text)
    text = re.sub("\[.*\]", "", text)

    wortliste = text.lower().split(" ")
    wortliste_neu = []
    for wort in wortliste:
        wortliste_neu.extend(wort.split("\n"))

    wortliste_cleaned = []

    for wort in wortliste_neu:
        wortliste_cleaned.append(wort.strip(".,\"()\'?!\n:;…_"))
    wortliste_cleaned = [wort for wort in wortliste_cleaned if wort != '']

    return wortliste_cleaned


# clean_text-Funktion auf alle Texte anwenden

for geschlecht, textdictionary in texte.items():
    for dateiname, textstring in textdictionary.items():
        textdictionary[dateiname] = clean_text(textstring)


# Funktion, die nach Wörtern in Liste filtert:

def position_farbwort(wortliste: list[str]) -> list[int]:
    farbliste = ["rot", "blau", "gelb", "grün", "orange", "violett", "lila", "pink", "weiß", "schwarz"]
    position = []
    for index, wort in enumerate(wortliste, start=0):
        if wort in farbliste:
            position.append(index)
    return position

# funktion position_farbwort für alle texte anwenden und in neuem dict abspeichern
farbwort_positions_dict = {
    "maennlich": {},
    "weiblich": {},
}
for geschlecht, token_dict in texte.items():
    for dateiname, tokenliste in token_dict.items():
        farbwort_positions_dict[geschlecht][dateiname] = position_farbwort(tokenliste)


# Anzahldatei schreiben
for geschlecht, farbwort_positions_dict_fuer_geschlecht in farbwort_positions_dict.items():
    _folder_ = r"D:\Daten\Python_Projekte\fd_abstrakte-nomen"
    with open(_folder_+r"anzahl_farben_"+geschlecht.replace(".","").replace(" ","_") + ".txt", "w", encoding="utf-8") as f:
        for dateiname, positionen in farbwort_positions_dict_fuer_geschlecht.items():
            f.write(f"{dateiname}: {len(positionen)}\n")


# ----------------------------------------------------------------------------------------------------------------------
# Plot 1: Anzahl vorkommende Emotionswörter je Geschlecht
# ----------------------------------------------------------------------------------------------------------------------
def plot_1():
    anzahl_farbwort_geschlecht = {}
    for geschlecht, positions_dict in farbwort_positions_dict.items():
        anzahl_farbwort_geschlecht[geschlecht] = 0
        for position in positions_dict.values():
            anzahl_farbwort_geschlecht[geschlecht] += len(position)

    fig, ax = plt.subplots()

    geschlecht = farbwort_positions_dict.keys()
    anzahl_farbwort_geschlecht = anzahl_farbwort_geschlecht.values()

    bars = ax.bar(geschlecht, anzahl_farbwort_geschlecht)
    ax.bar_label(bars, label_type="center")
    ax.tick_params(axis="y", which="both", direction="in", left=True, right=True)
    ax.tick_params(axis="y", which="major", bottom=True, top=True)
    ax.set_title('Wie oft kommen Farben je Geschlecht vor?' )

    plt.show()


#plot_1() # Plot ausführen
# ----------------------------------------------------------------------------------------------------------------------
# Plot Ende
# ----------------------------------------------------------------------------------------------------------------------

# Relative Anzahl Emotionswörter pro Gedicht je Geschlecht
def plot2():
    anzahl_farbwort_geschlecht = {}
    for geschlecht, positions_dict in farbwort_positions_dict.items():
        anzahl_farbwort_geschlecht[geschlecht] = 0
        for position in positions_dict.values():
            anzahl_farbwort_geschlecht[geschlecht] += len(position)

    anzahl_texte_m = len(texte["maennlich"])
    anzahl_texte_w = len(texte["weiblich"])

    fig, ax = plt.subplots()

    geschlecht = farbwort_positions_dict.keys()
    anzahl_farbwort_geschlecht = list(anzahl_farbwort_geschlecht.values())
    anzahl_farbwort_geschlecht[0] /= anzahl_texte_m
    anzahl_farbwort_geschlecht[1] /= anzahl_texte_w

    bars = ax.bar(geschlecht, anzahl_farbwort_geschlecht)
    ax.bar_label(bars, label_type="center")
    ax.tick_params(axis="y", which="both", direction="in", left=True, right=True)
    ax.tick_params(axis="y", which="major", bottom=True, top=True)
    ax.set_title('Wie oft kommen Farben je Geschlecht pro Text vor?')

    plt.show()


#plot2()  # Plot ausführen

#Texte je Emotionswort durchsuchen

def suche_farbwort(texte: dict[str, list[str]]) -> dict[str, int]:

    farben = ["rot", "blau", "gelb", "grün", "orange", "violett", "lila", "pink", "weiß", "schwarz"]

    counter_emotionswoerter_je_wort = {}

    for ew in farben:
        for text in texte.values():
            anzahl = text.count(ew)
            counter_emotionswoerter_je_wort[ew] = anzahl + counter_emotionswoerter_je_wort.get(ew, 0)

    return counter_emotionswoerter_je_wort

anzahl_maennlich = suche_farbwort(texte["maennlich"])
anzahl_weiblich = suche_farbwort(texte["weiblich"])



def plot_3():
    fig, ax = plt.subplots()

    anzahl_texte_m = len(texte["maennlich"])
    anzahl_texte_m = 1

    labels = anzahl_maennlich.keys()
    values = [wert / anzahl_texte_m for wert in anzahl_maennlich.values()]

    bars = ax.bar(labels, values)
    ax.bar_label(bars, label_type="center")
    ax.tick_params(axis="y", which="both", direction="in", left=True, right=True)
    ax.tick_params(axis="y", which="major", bottom=True, top=True)
    ax.set_title('Anzahl der Farben bei männlichen Autoren')

    plt.show()


plot_3()



def plot_4():
    fig, ax = plt.subplots()

    anzahl_texte_w = len(texte["weiblich"])
    anzahl_texte_w = 1

    labels = anzahl_weiblich.keys()
    values = [wert / anzahl_texte_w for wert in anzahl_weiblich.values()]

    bars = ax.bar(labels, values)
    ax.bar_label(bars, label_type="center")
    ax.tick_params(axis="y", which="both", direction="in", left=True, right=True)
    ax.tick_params(axis="y", which="major", bottom=True, top=True)
    ax.set_title('Anzahl der Farben bei weiblichen Autorinnen')

    plt.show()


plot_4()

# Relatives Auftreten : Anzahl Vorkommen Farben / Gesamtanzahl Tokens

anzahl_tokens_m = 0

for tokens in texte["maennlich"].values():
    anzahl_tokens_m += len(tokens)

print(anzahl_tokens_m)

anzahl_tokens_w = 0

for tokens in texte["weiblich"].values():
    anzahl_tokens_w += len(tokens)

print(anzahl_tokens_w)

def plot5():
    fig, ax = plt.subplots()



    labels = anzahl_maennlich.keys()
    values = [round(wert *100  / anzahl_tokens_m,4) for wert in anzahl_maennlich.values()]

    bars = ax.bar(labels, values)
    ax.bar_label(bars, label_type="center")
    ax.tick_params(axis="y", which="both", direction="in", left=True, right=True)
    ax.tick_params(axis="y", which="major", bottom=True, top=True)
    ax.set_title('Anzahl der Farben bei männlichen Autoren in Prozent')

    print(sum(values))
    plt.show()


#plot5()

def plot_6():
    fig, ax = plt.subplots()



    labels = anzahl_weiblich.keys()
    values = [round(wert *100 / anzahl_tokens_w,4) for wert in anzahl_weiblich.values()]

    bars = ax.bar(labels, values)
    ax.bar_label(bars, label_type="center")
    ax.tick_params(axis="y", which="both", direction="in", left=True, right=True)
    ax.tick_params(axis="y", which="major", bottom=True, top=True)
    ax.set_title('Anzahl der Farbwörter bei weiblichen Autorinnen in Prozent')

    print(sum(values))
    plt.show()


#plot_6()

# Plot, der relative Häufigkeit der Farben vergleicht:
def plot7():
    fig, ax = plt.subplots()



    labels = ["männliche Autoren", "weibliche Autoren"]
    values = [0.0892, 0.0833] #hier müssen Werte aus Konsole eingesetzt werden, die in den beiden Plots zuvor berechnet wurden

    bars = ax.bar(labels, values)
    ax.bar_label(bars, label_type="center")
    ax.tick_params(axis="y", which="both", direction="in", left=True, right=True)
    ax.tick_params(axis="y", which="major", bottom=True, top=True)
    ax.set_title('Vergleich prozentualer Anteil bezogen auf Gesamtanzahl der Tokens')


    plt.show()

#plot7()